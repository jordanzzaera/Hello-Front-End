module.exports = {
    entry: ['./Hello.ts'],
    output: {
        filename: 'bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                /*
                babel js(es6 a es5)
                ts-loader (typescript to js em6)                
                */
                loader: [
                    'babel-loader',
                    'awesome-typescript-loader',
                ]
            },
        ]
    }
   
};